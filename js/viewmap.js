var ctxArray = [];
            var ctxArrayRegions = [];
            var firstDraw = true;

            var firstTime = 1;
            var firstTimeState = 1;
            var map;
            var southWest;
            var northEast;
            var bounds;

            var canvasTiles = L.tileLayer.canvas();
            var owner;
            var jAssets = {};
            var regions = {};
            var countries = {};
            var parksMap = {};
            var polygons = {};
            var ownerCircle;
            var parkCircle;
            var regionCircle;
            var eolics;
            var solars;
            var hydrics;
            var assets;
            var eolicParksIcon = L.divIcon({
                className: 'park-icon-wpp marker-cluster-eolicPark'
            });
            var solarParksIcon = L.divIcon({
                className: 'park-icon-spp marker-cluster-solarPark'
            });
            var hydroParksIcon = L.divIcon({
                className: 'park-icon-hpp marker-cluster-hydroPark'
            });
            var eolicIcon = L.divIcon({
                className: 'park-icon-wtg marker-cluster-eolic'
            });

            var ownerCircleLayer;
            var parkCirclesLayer;
            var regionCirclesLayer;
			var ownersLayer;
			var countriesLayer;
			var regionsLayer;
            var addedOwnerCircle = false;
            var addedCountryCircle = false;
            var addedParkCircles = false;
            var addedRegionCircles = false;
            var addedPolygons = false;
            var parksExists = false;
            var addedAssets = false;
            var ownerPopup;
            var regionsPopups = [];
            var parksPopups = [];
            var multiPolygon;
            var polygonGroup;

            var ownerGroup = [];
            var countryGroup = [];
            var regionGroup = [];
            var parkGroup = [];


            var markersClick = [];
            var popupsClick = [];

            var isReady = 0;


            var setAssetsZoom;
            var setParkCircleZoom;
            var setParkIconMin;
            var setParkIconMax;
            var setRegionCircleMin;
            var setRegionCircleMax;
            var setCountryCircleMin;
            var setCountryCircleMax;
            var ownerCircleMin;
            var ownerCircleMax;

            function init() {
                // Hack to keep more than one popup open
                L.Map = L.Map.extend({
                    openPopup: function (popup) {
                        //this.closePopup();  // just comment this
                        this._popup = popup;

                        return this.addLayer(popup).fire('popupopen', {
                            popup: this._popup
                        });
                    }
                });

                map = L.map('leafletMap', {
                    touchZoom: true,
                    dragging: true,
                    zoomControl: false,
                    closePopupOnClick: false
                }).setView([39.678, -8.229], 5);

                // Initialize the SVG layer 
                //map._initPathRoot();

                // SVG from the map object 
                //var svg = d3.select("#map").select("svg");

                // Map bounds to prevent user from get out 
                southWest = [85.05, -180.01];
                northEast = [-85.06, 180.05];

                bounds = L.latLngBounds(southWest, northEast);

                map.setMaxBounds(bounds);
                // Tile properties 
                L.tileLayer('http://api.tiles.mapbox.com/v4/bmpgp.010fd6a5/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYm1wZ3AiLCJhIjoiOWY4NGYwN2VjZDg0MGI1ZjdmMWI3ZjdlNGNmY2NmNmQifQ.FZ5cr4mO3iDKVkx9zz4Nkg', {
                    attribution: 'RMS � Powered By <a href="http://www.cgi.com">CGI</a>',
                    minZoom: 4,
                    maxZoom: 18,
                    id: 'bmpgp.010fd6a5',
                    accessToken: 'pk.eyJ1IjoiYm1wZ3AiLCJhIjoiOWY4NGYwN2VjZDg0MGI1ZjdmMWI3ZjdlNGNmY2NmNmQifQ.FZ5cr4mO3iDKVkx9zz4Nkg'
                }).addTo(map);

                PolygonsConstruction();

				ownersLayer =new L.MarkerClusterGroup({
                    spiderfyOnMaxZoom: false,
                    disableClusteringAtZoom: 4
                });
				
				countriesLayer =new L.MarkerClusterGroup({
                    spiderfyOnMaxZoom: false,
                    disableClusteringAtZoom: 4
                });
				
				regionsLayer =new L.MarkerClusterGroup({
                    spiderfyOnMaxZoom: false,
                    disableClusteringAtZoom: 4
                });
				
                assets = new L.MarkerClusterGroup({
                    spiderfyOnMaxZoom: false,
                    disableClusteringAtZoom: 4
                });

                eolics = new L.MarkerClusterGroup({
                    spiderfyOnMaxZoom: true,
                    disableClusteringAtZoom: 11,
                    maxClusterRadius: 130,
                    iconCreateFunction: function (cluster) {
                        return L.divIcon({
                            className: 'marker-cluster marker-cluster-small',
                            html: '<div><span>' + cluster.getChildCount() + '</span></div>'
                        });
                    }
                });
                solars = new L.MarkerClusterGroup({
                    spiderfyOnMaxZoom: true,
                    disableClusteringAtZoom: 11,
                    maxClusterRadius: 130,
                    iconCreateFunction: function (cluster) {
                        return L.divIcon({
                            className: 'marker-cluster marker-cluster-small',
                            html: '<div><span>' + cluster.getChildCount() + '</span></div>'
                        });
                    }
                });
                hydrics = new L.MarkerClusterGroup({
                    spiderfyOnMaxZoom: true,
                    disableClusteringAtZoom: 11,
                    maxClusterRadius: 130,
                    iconCreateFunction: function (cluster) {
                        return L.divIcon({
                            className: 'marker-cluster marker-cluster-small',
                            html: '<div><span>' + cluster.getChildCount() + '</span></div>'
                        });
                    }
                });

                //markerCircle._icon.style.display = 'none';

                //Zoom End function 
                // 1: setAssetsZoom, 2: setParkCircleZoom, 3: setParkIconStart, 4: setParkIconStop, 5: setRegionCircleStart, 6: setRegionCircleStop, 7: OwnerCircleStart, 8: OwnerCircleStop 12, 12, 4, 12, 7, 11, 4, 7
                map.on('zoomend', onZoomEnd);
            }

            function OwnerConstruction(jOwner) {
                var ownerRadius = 300000; //jOwner.map.radius;
                if (ownerRadius < 100000) {
                    ownerRadius = 300000;
                }

                owner = {
                    id: jOwner.id,
                    lat: jOwner.map.lat,
                    lon: jOwner.map.long,
                    radius: ownerRadius,
                    name: jOwner.name,
                    aP: jOwner.aP,
                    lF: jOwner.lF,
                    weather: jOwner.weather,
                    type: jOwner.type
                };
            }

            function RegionConstruction(jRegion) {
                var regionRadius = jRegion.map.radius;
                if (regionRadius < 100000) {
                    regionRadius = 100000;
                }
                regions['' + jRegion.name + ''] = {
                    id: jRegion.id,
                    lat: jRegion.map.lat,
                    lon: jRegion.map.long,
                    radius: regionRadius,
                    color: '#FC7E3F',
                    name: jRegion.name,
                    aP: jRegion.aP,
                    lF: jRegion.lF,
                    weather: jRegion.weather,
                    type: jRegion.type
                }
            };

            function ParkConstruction(jPark, loc, jAssets) {
                parksMap['' + jPark.name + ''] = {
                    id: jPark.id,
                    name: jPark.name,
                    lat: jPark.map.lat,
                    lon: jPark.map.long,
                    loc: loc,
                    parkType: jPark.parkType,
                    type: jPark.type,
                    aP: jPark.aP,
                    lF: jPark.lF,
                    radius: jPark.map.radius, //CircleRadiusCalculation(jPark.map.lat, jPark.map.long, jAssets),
                    color: '#FC7E3F',
                    assets: jAssets,
                    kpis: jPark.kpis,
                    iP: jPark.iP,
                    mtdProd: jPark.mtdProd,
                    ytdProd: jPark.ytdProd,
                    weather: jPark.weather,
                    avl: jPark.avl,
                    wD: jPark.wD,
                    wS: jPark.wS,
                    eff: jPark.eff,
                    type: jPark.type
                }
            };

            function AssetConstruction(jAssets, jAsset) {
                jAssets['' + jAsset.id + ''] = {
                    id: jAsset.id,
                    name: jAsset.id,
                    lat: jAsset.lat,
                    lon: jAsset.long,
                    aP: jAsset.aP,
                    lF: jAsset.lF,
                    kpis: jAsset.kpis,
                    type: jAsset.type,
                    assetType: jAsset.assetType,
                    kpis: jAsset.kpis,
                    iP: jAsset.iP,
                    parent: jAsset.parent,
                    eff: jAsset.eff,
                    status: jAsset.status,
                    avl: jAsset.avl,
                    wD: jAsset.wD,
                    wS: jAsset.wS,
                    airTemp: jAsset.airTemp,
                    type: jAsset.type
                };
            };

            // Calculates the farthest asset to set the Circle Radius 
            function CircleRadiusCalculation(parkLat, parkLon, jAssets) {
                var parkCoordinates = [parkLat, parkLon];
                var mostFaraway = 0,
                    distance = 0;

                for (var ass in jAssets) {
                    var coordinates = L.latLng(jAssets[ass].lat, jAssets[ass].lon);
                    distance = coordinates.distanceTo(parkCoordinates);
                    if (mostFaraway < distance) {
                        mostFaraway = distance;
                    }
                }
                if (mostFaraway === 0) {
                    mostFaraway = 1000;
                }
                return mostFaraway;
            };

            function PolygonsConstruction() {
                for (var p in parksMap) {
                    polygons['' + parksMap[p].name + ''] = {};
                    var k = 0;

                    for (var a in parksMap[p].assets) {
                        polygons['' + parksMap[p].name + ''][k] = new L.LatLng(parksMap[p].assets[a].lat, parksMap[p].assets[a].lon);
                        k++;
                    }
                }
            }

            // Parks Insertion 

            function setParks() {
                for (var park in parksMap) {

                    var circle = new L.Icon.Canvas({
                        iconSize: new L.Point(50, 50),
                        className: 'parks-icons'
                    });

                    var item = parksMap[park];
                    var total = item.kpis.circleStatus.on + item.kpis.circleStatus.off + item.kpis.circleStatus.noComm;
                    var greenSize = item.kpis.circleStatus.on * 2 / total;
                    var redSize = item.kpis.circleStatus.off * 2 / total;

                    circle.draw = (function (item, total, greenSize, redSize) {
                        return function (ctx, w, h) {

                            setUpParkForStatus(item, ctx, greenSize, redSize);

                            switch (item.parkType) {
                            case 'WIND':
                                parkWindConstructor(ctx);
                                break;
                            case 'SOLAR':
                                parkSolarConstructor(ctx);
                                break;
                            case 'HYDRO':
                                parkHydroConstructor(ctx);
                                break;
                            }

                            ctxArray.push({
                                id: item.id,
                                ctx: ctx
                            });
                        };
                    })(item, total, greenSize, redSize);
	
	
                    var popup = L.popup({
                            closeButton: false,
                            closeOnClick: true,
                            autoPan: false,
                            className: 'leaflet-popup-park showLabel',
                            id: parksMap[park].id,
                            className: 'park-canvas-popup'
                        })
                        .setLatLng([parksMap[park].lat, parksMap[park].lon])
                        .setContent('<p><div id="' + parksMap[park].id + 'POP" style="box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 143px; position: relative; margin-left: 46px; z-index: 400; top: 20px" href="callback:' + parksMap[park].id + '"><div style="color: white; background-color: black; width: 120px; margin-left: 40px; margin-top: -16px; padding-top: 1px;"><span style="padding-left: 14px">' + parksMap[park].name + '</span></div><div style="width: 124px; background-color: white; margin-left: 35px; font-size: 10.2px;"><span style="padding-left: 19px"> LF: <span id="' + parksMap[park].id + 'LF">' + Math.round(parksMap[park].lF.tot) + '</span>% <span style="padding-left: 14px">AP: <span id="' + parksMap[park].id + 'AP">' + Math.round(parksMap[park].aP.tot) + '</span>MW</span></div></div>');

                    popupsClick.push({
                        id: parksMap[park].id,
                        popup: popup
                    });

                    var marker = new L.Marker([parksMap[park].lat, parksMap[park].lon], {
                        icon: circle,
                        draggable: false,
                        opacity: 1,
                        id: parksMap[park].id,
                        title: parksMap[park].id,
                        element: parksMap[park],
                        className: 'park-canvas-popup'
                    }).on('click', popupsManage);

                    markersClick.push({
                        marker: marker
                    });

                    switch (parksMap[park].parkType) {
                    case 'WIND':
                        eolics.addLayer(marker.bindPopup(popup));

                        break;
                    case 'SOLAR':
                        solars.addLayer(marker.bindPopup(popup));

                        break;
                    case 'HYDRO':
                        hydrics.addLayer(marker.bindPopup(popup));

                        break;
                    }


                }
                map.addLayer(eolics);
                map.addLayer(solars);
                map.addLayer(hydrics);

                parksExists = true;
            };

            function setOwnerCircle() {
				
                createCircle(owner);
				//setOwner();

                //map.addLayer(ownerCircleLayer);

                addedOwnerCircle = true;
                ownerGroup.push({
                    'circle': ownerCircle,
                    'popup': ownerPopup,
                    'name': owner.name,
                    'id': owner.id
                });
            };

			function setOwner() {
				var circle = new L.Icon.Canvas({
                        iconSize: new L.Point(50, 50),
                        className: 'owner-icon'
                    });

                    circle.draw =  function (ctx, w, h) {

						 ctx.clearRect(0, 0, 50, 50);
                             drawArc(ctx, 0, 2, '#CCCCCC');
							 var img = new Image();
img.onload = function () {
    ctx.drawImage(img, 7, 8, 35, 35);
}
img.src = "img/" + owner.id + "_icon.png";
							 
                            ctxArray.push({
                                id: owner.id + 'icon',
                                ctx: ctx
                            });
                        };
	
                    var popup = L.popup({
                            closeButton: false,
                            closeOnClick: true,
                            autoPan: false,
                            className: 'leaflet-popup-owner showLabel',
                            id: owner.id,
                            className: 'owner-canvas-popup'
                        })
                        .setLatLng([owner.lat, owner.lon])
                        .setContent('<p><div id="' + owner.id + 'POP" style="box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 143px; position: relative; margin-left: 37px; z-index: 400; top: 20px" href="callback:' + owner.id + '"><div style="color: white; background-color: black; width: 130px; margin-left: 40px; margin-top: -16px; padding-top: 1px;"><span style="padding-left: 14px">' + owner.name + '</span></div><div style="width: 134px; background-color: white; margin-left: 35px; font-size: 10.2px;"><span style="padding-left: 19px"> LF: <span id="' + owner.id + 'LF">' + Math.round(owner.lF.tot) + '</span>% <span style="padding-left: 14px">AP: <span id="' + owner.id + 'AP">' + Math.round(owner.aP.tot) + '</span>MW</span></div></div>');

                    popupsClick.push({
                        id: owner.id,
                        popup: popup
                    });

                    var marker = new L.Marker([owner.lat, owner.lon], {
                        icon: circle,
                        draggable: false,
                        opacity: 1,
                        id: owner.id,
                        title: owner.id,
                        element: owner,
                        className: 'owner-canvas-popup'
                    }).on('click', popupsManage);

                    markersClick.push({
                        marker: marker
                    });
					ownersLayer.addLayer(marker.bindPopup(popup));
					                map.addLayer(ownersLayer);

			}
			
			function setCountryCircle() {
                for (var country in countries) {
                    createCircle(countries[country]);

                    addedCountryCircle = true;
                    countryGroup.push({
                        'name': countries[country].name,
                        'id': countries[country].id
                    });
					//setCountries(countries[country]);
                }
            }
			
			function setCountries(country) {

				var circle = new L.Icon.Canvas({
                        iconSize: new L.Point(50, 50),
                        className: 'country-icon'
                    });

					var item = country;
                    var total = item.kpis.circleStatus.on + item.kpis.circleStatus.off + item.kpis.circleStatus.noComm;
                    var greenSize = item.kpis.circleStatus.on * 2 / total;
                    var redSize = item.kpis.circleStatus.off * 2 / total;
					
                    circle.draw =  (function (item, total, greenSize, redSize) {
                        return function (ctx, w, h) {

					var distance;
					if(country.acc.length === 1) {
						distance = 17;
					} else {
						distance = 11;
					}
					     setUpParkForStatus(item, ctx, greenSize, redSize);

							 
				ctx.font = "16pt Helvetica-Th";
                ctx.fillStyle = "black";
                ctx.textAlign = "middle";
                ctx.fillText(country.acc, distance, 35);
							 
                            ctxArray.push({
                                id: country.id + 'icon',
                                ctx: ctx
                            });
                        };


                    })(item, total, greenSize, redSize);

	
                    var popup = L.popup({
                            closeButton: false,
                            closeOnClick: true,
                            autoPan: false,
                            className: 'leaflet-popup-country showLabel',
                            id: country.id,
                            className: 'country-canvas-popup'
                        })
                        .setLatLng([country.lat, country.lon])
                        .setContent('<p><div id="' + country.id + 'POP" style="box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 143px; position: relative; margin-left: 37px; z-index: 400; top: 20px" href="callback:' + country.id + '"><div style="color: white; background-color: black; width: 130px; margin-left: 40px; margin-top: -16px; padding-top: 1px;"><span style="padding-left: 14px">' + country.name + '</span></div><div style="width: 134px; background-color: white; margin-left: 35px; font-size: 10.2px;"><span style="padding-left: 19px"> LF: <span id="' + country.id + 'LF">' + Math.round(country.lF.tot) + '</span>% <span style="padding-left: 14px">AP: <span id="' + country.id + 'AP">' + Math.round(country.aP.tot) + '</span>MW</span></div></div>');

                    popupsClick.push({
                        id: country.id,
                        popup: popup
                    });

                    var marker = new L.Marker([country.lat, country.lon], {
                        icon: circle,
                        draggable: false,
                        opacity: 1,
                        id: country.id,
                        title: country.id,
                        element: country,
                        className: 'country-canvas-popup'
                    }).on('click', popupsManage);

                    markersClick.push({
                        marker: marker
                    });
					countriesLayer.addLayer(marker.bindPopup(popup));
					                map.addLayer(countriesLayer);

			}
			
            // Assets insertion 
            function setAssets() {
                map.addLayer(assets);

                for (var park in parksMap) {
                    for (var asset in parksMap[park].assets) {

                        var color;

                        switch (parksMap[park].assets[asset].status) {
                        case 100: // run
                            color = '#39B54A';
                            break;
                        case 200: // ready
                            color = '#0071BC';
                            break;
                        case 300: // stop
                            color = '#C1272D';
                            break;
                        case 400: // maint
                            color = '#FCEE21';
                            break;
                        case 500: // restricted
                            color = '#F7931E';
                            break;
                        case 600:
                        case 301: // repair
                            color = '#A67C52';
                            break;
                        default: // nocomm
                            color = '#D927BF';
                            break;
                        }


                        var circle = new L.Icon.Canvas({
                            iconSize: new L.Point(50, 50),
                            className: 'asset-icons'
                        });

                        var item = parksMap[park].assets[asset];
                        var itemPark = parksMap[park];

                        circle.draw = (function (item, itemPark, color) {
                            return function (ctx, w, h) {
                                ctx.clearRect(0, 0, 50, 50);
                                drawArc(ctx, 0, 2, color);
                                ctxArray.push({
                                    id: item.id,
                                    ctx: ctx
                                });

                                switch (itemPark.parkType) {
                                case 'WIND':
                                    assetWindConstructor(ctx);
                                    break;
                                case 'SOLAR':
                                    assetSolarConstructor(ctx)
                                    break;
                                case 'HYDRO':
                                    assetHydroConstructor(ctx)
                                    break;

                                }
                            }
                        })(item, itemPark, color);

                        var popup = L.popup({
                                closeButton: false,
                                closeOnClick: true,
                                autoPan: false,
                                className: 'asset-popup'
                            })
                            .setLatLng([parksMap[park].assets[asset].lat, parksMap[park].assets[asset].lon])
                            .setContent('<p><div id="' + parksMap[park].assets[asset].id + 'POP" class="asset-marker" style="box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 113px; position: absolute; top: 5px; left: 5px; margin-top: 16px; margin-left: 80px;" href="callback:' + parksMap[park].assets[asset].id + '"><div id="' + parksMap[park].assets[asset].id + 'BAR" style="color: white; background-color:' + color + '; width: 130px; margin-top: -16px; padding-top: 1px; margin-left: 6px;"><span style="padding-left: 16px">' + parksMap[park].assets[asset].id + '</span></div><div style="width: 129px; background-color: white; font-size: 10.2px; margin-left: 6px; padding-left: 20px;">LF: <span id="' + parksMap[park].assets[asset].id + 'LF">' + Math.round(parksMap[park].assets[asset].lF.tot) + '</span>% <span style="padding-left: 14px">AP: <span id="' + parksMap[park].assets[asset].id + 'AP">' + Math.round(parksMap[park].assets[asset].aP.tot) + '</span>MW </span></span></div></div>')

                        popupsClick.push({
                            popup: popup,
                            id: parksMap[park].assets[asset].id
                        });

                        var marker = new L.Marker([parksMap[park].assets[asset].lat, parksMap[park].assets[asset].lon], {
                            icon: circle,
                            draggable: false,
                            opacity: 1,
                            id: parksMap[park].assets[asset].id,
                            title: parksMap[park].assets[asset].id,
                            element: parksMap[park].assets[asset],
                        }).on('click', popupsManage);

                        assets.addLayer(marker.bindPopup(popup));

                        markersClick.push({
                            marker: marker
                        });

                    }
                }
                addedAssets = true;
            }

            // Set Parks' Circles 
            function setParksCircles() {
                if (addedParkCircles != true) {
                    parkCirclesLayer = new L.layerGroup();

                    for (var park in parksMap) {
                        createCircle(parksMap[park]);

                    }

                    map.addLayer(parkCirclesLayer);

                    addedParkCircles = true;
                }
            }

            // Set Regions' Circles 
            function setRegionsCircles() {
				
                if (addedRegionCircles != true) {
                    regionCirclesLayer = new L.layerGroup();

                    var i = 0;

                    for (var reg in regions) {
                    
                    createCircle(regions[reg]);
										setRegions(regions[reg]);

					}

                    //map.addLayer(regionCirclesLayer);

                    addedRegionCircles = true;

                }
            }
	
			function setRegions(region) {
				var circle = new L.Icon.Canvas({
                        iconSize: new L.Point(50, 50),
                        className: 'region-icon'
                    });

					var item = region;
                    var total = item.kpis.circleStatus.on + item.kpis.circleStatus.off + item.kpis.circleStatus.noComm;
                    var greenSize = item.kpis.circleStatus.on * 2 / total;
                    var redSize = item.kpis.circleStatus.off * 2 / total;
					
                    circle.draw =  (function (item, total, greenSize, redSize) {
                        return function (ctx, w, h) {

					var distance;
					if(region.acc.length === 1) {
						distance = 17;
					} else {
						distance = 11;
					}
					     setUpParkForStatus(item, ctx, greenSize, redSize);

							 
				ctx.font = "16pt Helvetica-Th";
                ctx.fillStyle = "black";
                ctx.textAlign = "middle";
                ctx.fillText(region.acc, distance, 35);
							 
                            ctxArray.push({
                                id: region.id + 'icon',
                                ctx: ctx
                            });
                        };


                    })(item, total, greenSize, redSize);

	
                    var popup = L.popup({
                            closeButton: false,
                            closeOnClick: true,
                            autoPan: false,
                            className: 'leaflet-popup-region showLabel',
                            id: region.id,
                            className: 'region-canvas-popup'
                        })
                        .setLatLng([region.lat, region.lon])
                        .setContent('<p><div id="' + region.id + 'POP" style="box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 143px; position: relative; margin-left: 37px; z-index: 400; top: 20px" href="callback:' + region.id + '"><div style="color: white; background-color: black; width: 130px; margin-left: 40px; margin-top: -16px; padding-top: 1px;"><span style="padding-left: 14px">' + region.name + '</span></div><div style="width: 134px; background-color: white; margin-left: 35px; font-size: 10.2px;"><span style="padding-left: 19px"> LF: <span id="' + region.id + 'LF">' + Math.round(region.lF.tot) + '</span>% <span style="padding-left: 14px">AP: <span id="' + region.id + 'AP">' + Math.round(region.aP.tot) + '</span>MW</span></div></div>');

                    popupsClick.push({
                        id: region.id,
                        popup: popup
                    });

                    var marker = new L.Marker([region.lat, region.lon], {
                        icon: circle,
                        draggable: false,
                        opacity: 1,
                        id: region.id,
                        title: region.id,
                        element: region,
                        className: 'regions-canvas-popup'
                    }).on('click', popupsManage);

                    markersClick.push({
                        marker: marker
                    });
					regionsLayer.addLayer(marker.bindPopup(popup));
					                map.addLayer(regionsLayer);

			};
	
            // Parks' Polygons 
            function setPolygons() {
                multiPolygon = [];

                for (var poly in polygons) {
                    var pointsCount = 0;

                    for (var point in polygons[poly]) {
                        pointsCount++;
                    }

                    var polygonPoints = [];

                    for (var j = 0; j < pointsCount; j++) {
                        polygonPoints.push(polygons[poly][j]);
                    }

                    multiPolygon.push(polygonPoints);
                }

                polygonGroup = new L.multiPolygon(multiPolygon, {
                    color: '#FC7E3F',
                    weight: 3,
                    className: 'park-polygon'
                });
                map.addLayer(polygonGroup);

                addedPolygons = true;
            }

            function zoomDefinitions(SetAssetsZoom, SetParkCircleZoom, SetParkIconMin, SetParkIconMax, SetRegionCircleMin, SetRegionCircleMax, SetCountryCircleMin, SetCountryCircleMax, OwnerCircleMin, OwnerCircleMax) {

                setAssetsZoom = SetAssetsZoom;
                setParkCircleZoom = SetParkCircleZoom;
                setParkIconMin = SetParkIconMin;
                setParkIconMax = SetParkIconMax;
                setRegionCircleMin = SetRegionCircleMin;
                setRegionCircleMax = SetRegionCircleMax;
                setCountryCircleMin = SetCountryCircleMin;
                setCountryCircleMax = SetCountryCircleMax;
                ownerCircleMin = OwnerCircleMin;
                ownerCircleMax = OwnerCircleMax;

                onZoomEnd();
            }

            // On zoom 10 writes down park circles 
            function onZoomEnd(e) {
                var zoomLevel = GetZoomLevel();
				console.log(zoomLevel);

                if (addedOwnerCircle == true && zoomLevel > ownerCircleMax) {
                    //$('.owner-circle')[0].style.display = 'none';
                    $('.owner-label').css("display", "none");
                    $('.owner-icon').css("display", "none");
                    addedOwnerCircle = false;
                }

                if (addedOwnerCircle == false && zoomLevel <= ownerCircleMax) {
                    //$('.owner-circle')[0].style.display = 'inline-block';
                    $('.owner-label').css("display", "inline-block");
                    $('.owner-icon').css("display", "inline-block");
                    addedOwnerCircle = true;
                }
				
                if (addedCountryCircle == true && (zoomLevel > setCountryCircleMax || zoomLevel <= setCountryCircleMin)) {
                    //$('.region-circle').css("display", "none");
                    $('.country-label').css("display", "none");
                    $('.country-icon').css("display", "none");
                    addedCountryCircle = false;
                }

                if (addedCountryCircle == false && (zoomLevel > setCountryCircleMin && zoomLevel < setCountryCircleMax)){
                    //$('.region-circle').css("display", "inline-block");
                    $('.country-label').css("display", "inline-block");
                    $('.country-icon').css("display", "inline-block");

                    $('.country-label').remove();

                    for (var country in countries) {
                        createCircle(countries[country]);
                    }
                    addedCountryCircle = true;
                }

                if (addedRegionCircles == true && (zoomLevel >= setRegionCircleMax || zoomLevel <= setRegionCircleMin)) {
                    //$('.region-circle').css("display", "none");
                    $('.region-label').css("display", "none");
                    $('.region-icon').css("display", "none");
                    addedRegionCircles = false;
                }

                if (addedRegionCircles == false && (zoomLevel > setRegionCircleMin && zoomLevel <= setRegionCircleMax)) {
                    //$('.region-circle').css("display", "inline-block");
                    $('.region-label').css("display", "inline-block");
                    $('.region-icon').css("display", "inline-block");

                    $('.region-label').remove();

                    for (var reg in regions) {
                        createCircle(regions[reg]);
                    }
                    addedRegionCircles = true;
                }

                if (!parksExists && zoomLevel >= setParkIconMax) {
                    $('.parks-icons').css("display", "none");
                    if ($('.park-canvas-popup')) {
                        $('.park-canvas-popup').css("display", "none");
                    }
                    parksExists = true;
                }

                if (parksExists && zoomLevel < setParkIconMax) {
                    $('.parks-icons').css("display", "inline-block");
                    if ($('.park-canvas-popup')) {
                        $('.park-canvas-popup').css("display", "inline-block");
                    }
                    parksExists = false;
                }

                if (!addedAssets && zoomLevel >= setAssetsZoom) {
                    $('.asset-icons').css("display", "inline-block");
                    //$('.park-circle').css("display", "inline-block");
                    //$('.park-circle-popup').css("display", "inline-block");
                    $('.park-label').css("display", "inline-block");
                    $('.park-polygon').css("display", "inline-block");

                    if ($('.asset-popup')) {
                        $('.asset-popup').css("display", "inline-block");
                    }

                    $('.park-label').remove();

                    for (var park in parksMap) {
                        createCircle(parksMap[park]);
                    }

                    parksExists = true;
                    addedAssets = true;
                }

                if (zoomLevel < setAssetsZoom) {
                    $('.asset-icons').css("display", "none");
                    //$('.park-circle').css("display", "none");
                    //$('.park-circle-popup').css("display", "none");
                    $('.park-label').css("display", "none");
                    $('.park-polygon').css("display", "none");

                    if ($('.asset-popup')) {
                        $('.asset-popup').css("display", "none");
                    }

                    addedAssets = false;
                    parksExists = false;
                }

                setAnglesZoom(zoomLevel);
                //ChangeZIndex();
            }

            function setAnglesZoom(zoom) {
				
				if(typeof owner !== 'undefined') {
                if (zoom < ownerCircleMax && zoom >= ownerCircleMin) {
                    $('.owner-label').remove();

                    createCircle(owner);
                }
				}
            };


            // Get Current zoom level 
            function GetZoomLevel() {
                return map.getZoom();
            };

            function createCircle(item) {
                var className;
                var label;

                if (item.type === 'owner') {
                    $('.owner-label').remove();

                    label = new L.Icon.Canvas({
                        iconSize: new L.Point(10000, 6000),
                        className: 'owner-label'
                    });

                    className = 'owner-canvas-popup'
                } else if (item.type === 'country') {
                    label = new L.Icon.Canvas({
                        iconSize: new L.Point(10000, 6000),
                        className: 'country-label country-label-' + item.id
                    });

                    className = 'country-canvas-popup'

                } else if (item.type === 'region') {
                    label = new L.Icon.Canvas({
                        iconSize: new L.Point(10000, 6000),
                        className: 'region-label region-label-' + item.id
                    });

                    className = 'region-canvas-popup'
                } else {
                    label = new L.Icon.Canvas({
                        iconSize: new L.Point(10000, 6000),
                        className: 'park-label park-label-' + item.id
                    });

                    className = 'park-canvas-popup'
                }

                var valueTranslate;
                switch (map.getZoom()) {
                case 4:
                    valueTranslate = 40;
                    break;
                case 5:
                    valueTranslate = 80;
                    break;
                case 6:
                    valueTranslate = 160;
                    break;
                case 7:
                    valueTranslate = 320;
                    break;
                case 8:
                    valueTranslate = 640;
                    break;
                case 9:
                    valueTranslate = 1280;
                    break;
                case 10:
                    valueTranslate = 2560;
                    break;
                case 11:
                    valueTranslate = 5120;
                    break;
                case 12:
                    valueTranslate = 10240;
                    break;
                case 13:
                    valueTranslate = 20480;
                    break;
                case 14:
                    valueTranslate = 40960;
                    break;
                case 15:
                    valueTranslate = 81920;
                    break;
                case 16:
                    valueTranslate = 163840;
                    break;
                case 17:
                    valueTranslate = 327680;
                    break;
                case 18:
                    valueTranslate = 655360;
                    break;
                }

                var valueRad = (item.radius * valueTranslate) / 300000;
                label.draw = function (ctx, w, h) {

                    ctx.clearRect(0, 0, 10000, 500);

                    var x = (w / 2);
                    var y = (h / 2);
                    var radius = valueRad;
                    var startAngle = 1.99 * Math.PI;
                    var endAngle = 0.01 * Math.PI;
                    var counterClockwise = false;

                    var xCalc = x + radius + 15;
                    var yCalc = y - 15;

                    var topX = x;
                    var topY = y - 15;

                    var value = Math.sqrt(Math.pow(radius, 2) - Math.pow(15, 2));

                    ctx.beginPath();

                    ctx.moveTo(x + value, y - 15);
                    ctx.lineTo(x + radius + 15, y - 15);

                    ctx.arc(x, y, radius, 0 * Math.PI, startAngle, true);

                    ctx.moveTo(x + radius + 15, y - 15);
                    ctx.lineTo(x + radius + 15, y);


                    ctx.fillStyle = '#FC7E3F';
                    ctx.strokeStyle = '#FC7E3F';
                    ctx.fill();
                    ctx.stroke();
                    ctx.closePath();

                    ctx.beginPath();
                    ctx.moveTo(x + value, y + 15);
                    ctx.lineTo(x + radius + 15, y + 15);

                    ctx.arc(x, y, radius, 0 * Math.PI, endAngle, false);

                    ctx.moveTo(x + radius + 15, y + 15);
                    ctx.lineTo(x + radius + 15, y);


                    ctx.strokeStyle = '#ffffff';

                    ctx.strokeStyle = '#ffffff';
                    ctx.fillStyle = '#ffffff';
                    ctx.fill();
                    ctx.stroke();
                    ctx.closePath();

                    ctx.beginPath();
                    ctx.arc(x, y, radius, 0 * Math.PI, endAngle, false);
                    ctx.strokeStyle = '#FC7E3F';
                    ctx.stroke();
                    ctx.closePath();

                    ctx.beginPath();
                    ctx.rect(x + radius, y - 16, 150, 15);
                    ctx.fillStyle = '#FC7E3F';
                    ctx.fill();

                    ctx.lineWidth = 1;
                    ctx.strokeStyle = '#FC7E3F';
                    ctx.stroke();

                    ctx.font = '12pt Helvetica-Cn';
                    ctx.fillStyle = '#ffffff';
                    ctx.fillText(item.name, x + radius + 20, y - 3);

                    ctx.closePath();

                    ctx.beginPath();
                    ctx.rect(x + radius + 2, y, 147, 15);
                    ctx.fillStyle = '#ffffff';
                    ctx.fill();
                    ctx.lineWidth = 1;
                    ctx.strokeStyle = '#ffffff';
                    ctx.stroke();

                    ctx.font = '9pt Helvetica-LtCn';
                    ctx.fillStyle = '#000000';
                    ctx.fillText('LF: ', x + radius + 20, y + 13);

                    ctx.font = '10pt Helvetica-BdCn';
                    ctx.fillStyle = '#000000';
                    ctx.fillText(Math.round(item.lF.tot), x + radius + 40, y + 13);

                    ctx.font = '9pt Helvetica-BdCn';
                    ctx.fillStyle = '#000000';
                    ctx.fillText('%', x + radius + 60, y + 13);

                    ctx.font = '9pt Helvetica-LtCn';
                    ctx.fillStyle = '#000000';
                    ctx.fillText('AP: ', x + radius + 80, y + 13);

                    ctx.font = '10pt Helvetica-BdCn';
                    ctx.fillStyle = '#000000';
                    ctx.fillText(Math.round(item.aP.tot), x + radius + 100, y + 13);

                    ctx.font = '9pt Helvetica-BdCn';
                    ctx.fillStyle = '#000000';
                    ctx.fillText('MW', x + radius + 125, y + 13);

                    ctx.closePath();

                    if (item.type !== 'owner') {
                        ctx.beginPath();
                        ctx.rect(x + radius + 150, y - 16, 32, 32);
                        ctx.fillStyle = '#FC7E3F';
                        ctx.fill();
                        ctx.closePath();

                        ctx.beginPath();
                        ctx.font = "18pt Meteo";
                        ctx.fillStyle = '#ffffff';
                        ctx.fillText(getWeather(item.weather), x + radius + 153, y + 12);
                        ctx.closePath();
                    }


                    ctx.beginPath();
                    ctx.arc(x, y, radius, 0 * Math.PI, 2 * Math.PI, counterClockwise);
                    ctx.lineWidth = 2;

                    // line color
                    ctx.strokeStyle = '#FC7E3F';
                    ctx.stroke();
                    ctx.closePath();

                    ctxArrayRegions.push({
                        id: item.id,
                        ctx: ctx
                    });
                };

                var markerCircle = new L.Marker([item.lat, item.lon], {
                    icon: label,
                    draggable: false,
                    opacity: 1,
                    id: item.id,
                    title: item.id,
                    element: item,
                    className: className
                }).addTo(map);

                markerCircle._icon.style.zIndex = '-1';
            };


            // Change Markers z-index 
            /*function ChangeZIndex() {
                map.eachLayer(function (marker) {
                    if (marker.options != null && marker.options.zIndexOffset != null) {
                        marker.setZIndexOffset(1000);
                    }
                });
            };*/

            function updateOwner(id, lf, ap) {
                owner.ap = ap;
                owner.aP.tot = ap;
                owner.lf = lf;
				owner.lF.tot = lf;

                if (map.getZoom() < ownerCircleMax && map.getZoom() >= ownerCircleMin) {
                    $('.owner-label').remove();
                    setOwnerCircle(owner);
                }
				
				if($('#' + id + 'POP').length > 0) {
				for (var k = 0; k < popupsClick.length; k++) {
                            if (popupsClick[k].id === id) {
                                popupsClick[k].popup.setContent('<p><div id="' + owner.id + 'POP" style="box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 143px; position: relative; margin-left: 37px; z-index: 400; top: 20px" href="callback:' + owner.id + '"><div style="color: white; background-color: black; width: 130px; margin-left: 40px; margin-top: -16px; padding-top: 1px;"><span style="padding-left: 14px">' + owner.name + '</span></div><div style="width: 134px; background-color: white; margin-left: 35px; font-size: 10.2px;"><span style="padding-left: 19px"> LF: <span id="' + owner.id + 'LF">' + Math.round(owner.lF.tot) + '</span>% <span style="padding-left: 14px">AP: <span id="' + owner.id + 'AP">' + Math.round(owner.aP.tot) + '</span>MW</span></div></div>')
                            }
                        }
						}
            };
			
			function updateCountry(id, lf, ap, online, offline, nocomm, weather) {
                for (var country in countries) {
                    if (id === countries[country].id) {
                        countries[country].ap = ap;
                        countries[country].aP.tot = ap;
                    
                        countries[country].lf = lf;
                        countries[country].lF.tot = lf;
                        
						countries[country].kpis.circleStatus.on = online;
						countries[country].kpis.circleStatus.off = offline;
						countries[country].kpis.circleStatus.noComm = nocomm;
                        countries[country].weather = weather;
                        
                        if (map.getZoom() < setCountryCircleMax && map.getZoom() >= setCountryCircleMin) {
                            $('.country-label-' + id).remove();
                            createCircle(countries[country]);
                        }
						
						for (var i = 0; i < ctxArray.length; i++) {
                            if (ctxArray[i].id === id + 'icon') {
                                var total = online + offline + nocomm;
                                var greenSize = online * 2 / total;
                                var redSize = offline * 2 / total;

                                setUpParkForStatus(countries[country], ctxArray[i].ctx, greenSize, redSize);

                                var distance;
					if(countries[country].acc.length === 1) {
						distance = 17;
					} else {
						distance = 11;
					}
							 
				ctxArray[i].ctx.font = "16pt Helvetica-Th";
                ctxArray[i].ctx.fillStyle = "black";
                ctxArray[i].ctx.textAlign = "middle";
                ctxArray[i].ctx.fillText(countries[country].acc, distance, 35);
                            }
                        }
						
				for (var k = 0; k < popupsClick.length; k++) {
                            if (popupsClick[k].id === id) {
                                popupsClick[k].popup.setContent('<p><div id="' + countries[country].id + 'POP" style="box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 143px; position: relative; margin-left: 37px; z-index: 400; top: 20px" href="callback:' + countries[country].id + '"><div style="color: white; background-color: black; width: 130px; margin-left: 40px; margin-top: -16px; padding-top: 1px;"><span style="padding-left: 14px">' + countries[country].name + '</span></div><div style="width: 134px; background-color: white; margin-left: 35px; font-size: 10.2px;"><span style="padding-left: 19px"> LF: <span id="' + countries[country].id + 'LF">' + Math.round(countries[country].lF.tot) + '</span>% <span style="padding-left: 14px">AP: <span id="' + countries[country].id + 'AP">' + Math.round(countries[country].aP.tot) + '</span>MW</span></div></div>')
                            }
                        }
                    }
                }
            };

            function updateRegion(id, lf, ap, online, offline, nocomm, weather) {
                for (var reg in regions) {
                    if (id === regions[reg].id) {
                        regions[reg].ap = ap;
						                        regions[reg].aP.tot = ap;

                        regions[reg].lf = lf;
						                        regions[reg].lF.tot = lf;

                        regions[reg].weather = weather;
						
                        if (map.getZoom() < setRegionCircleMax && map.getZoom() >= setRegionCircleMin) {
                            $('.region-label-' + id).remove();
                            createCircle(regions[reg]);
                        }
						
						for (var i = 0; i < ctxArray.length; i++) {
                            if (ctxArray[i].id === id + 'icon') {
                                var total = online + offline + nocomm;
                                var greenSize = online * 2 / total;
                                var redSize = offline * 2 / total;

                                setUpParkForStatus(regions[reg], ctxArray[i].ctx, greenSize, redSize);

                                var distance;
					if(regions[reg].acc.length === 1) {
						distance = 17;
					} else {
						distance = 11;
					}
							 
				ctxArray[i].ctx.font = "16pt Helvetica-Th";
                ctxArray[i].ctx.fillStyle = "black";
                ctxArray[i].ctx.textAlign = "middle";
                ctxArray[i].ctx.fillText(regions[reg].acc, distance, 35);
                            }
                        }
						
						for (var k = 0; k < popupsClick.length; k++) {
                            if (popupsClick[k].id === id) {
                                popupsClick[k].popup.setContent('<p><div id="' + regions[reg].id + 'POP" style="box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 143px; position: relative; margin-left: 37px; z-index: 400; top: 20px" href="callback:' + regions[reg].id + '"><div style="color: white; background-color: black; width: 130px; margin-left: 40px; margin-top: -16px; padding-top: 1px;"><span style="padding-left: 14px">' + regions[reg].name + '</span></div><div style="width: 134px; background-color: white; margin-left: 35px; font-size: 10.2px;"><span style="padding-left: 19px"> LF: <span id="' + regions[reg].id + 'LF">' + Math.round(regions[reg].lF.tot) + '</span>% <span style="padding-left: 14px">AP: <span id="' + regions[reg].id + 'AP">' + Math.round(regions[reg].aP.tot) + '</span>MW</span></div></div>')
                            }
                        }
						
                    }
                }
            };

            function updateAsset(id, lf, ap, status) {

                var color;
                switch (status) {
                case 100: // run
                    color = '#39B54A';
                    break;
                case 200: // ready
                    color = '#0071BC';
                    break;
                case 300: // stop
                    color = '#C1272D';
                    break;
                case 400: // maint
                    color = '#FCEE21';
                    break;
                case 500: // restricted
                    color = '#F7931E';
                    break;
                case 600:
                case 301: // repair
                    color = '#A67C52';
                    break;
                default: // nocomm
                    color = '#D927BF';
                    break;
                }

                jAssets[id].lf = lf;
                jAssets[id].ap = ap;
                jAssets[id].status = status;

                for (var i = 0; i < ctxArray.length; i++) {
                    if (ctxArray[i].id === id) {
                        ctxArray[i].ctx.clearRect(-50, -50, 50, 50);

                        drawArc(ctxArray[i].ctx, 0, 2, color);

                        if (jAssets[id].assetType === 'WTG') {
                            assetWindConstructor(ctxArray[i].ctx);
                        } else if (jAssets[id].assetType === 'INV') {
                            assetSolarConstructor(ctxArray[i].ctx);
                        } else {
                            assetHydroConstructor(ctxArray[i].ctx);
                        }
                    }
                }

                for (var k = 0; k < popupsClick.length; k++) {
                    if (popupsClick[k].id === id) {
                        popupsClick[k].popup.setContent('<p><div id="' + jAssets[id].id + 'POP" class="asset-marker" style="box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 113px; position: absolute; top: 5px; left: 5px; margin-top: 16px; margin-left: 80px;" href="callback:' + jAssets[id].id + '"><div id="' + jAssets[id].id + 'BAR" style="color: white; background-color:' + color + '; width: 130px; margin-top: -16px; padding-top: 1px; margin-left: 6px;"><span style="padding-left: 16px">' + jAssets[id].id + '</span></div><div style="width: 129px; background-color: white; font-size: 10.2px; margin-left: 6px; padding-left: 20px;">LF: <span id="' + jAssets[id].id + 'LF">' + Math.round(jAssets[id].lf) + '</span>% <span style="padding-left: 14px">AP: <span id="' + jAssets[id].id + 'AP">' + Math.round(jAssets[id].ap) + '</span>MW </span></span></div></div>');
                    }
                } // POP UP CLICKS END
            };

            function updatePark(id, lf, ap, weather, online, offline, nocomm) {

                for (var park in parksMap) {
                    if (parksMap[park].id === id) {
                        parksMap[park].lf = lf;
                        parksMap[park].ap = ap;
                        parksMap[park].kpis.circleStatus.on = online;
                        parksMap[park].kpis.circleStatus.off = offline;
                        parksMap[park].kpis.circleStatus.noComm = nocomm;
                        $('#' + id + 'ID #' + id + 'WEATHER').removeClass(parksMap[park].weather);
                        parksMap[park].weather = weather;
                        $('#' + id + 'ID #' + id + 'WEATHER').addClass(weather);

                        for (var i = 0; i < ctxArray.length; i++) {
                            if (ctxArray[i].id === id) {
                                var total = online + offline + nocomm;
                                var greenSize = online * 2 / total;
                                var redSize = offline * 2 / total;

                                setUpParkForStatus(parksMap[park], ctxArray[i].ctx, greenSize, redSize);

                                if (parksMap[park].parkType === 'WIND') {
                                    parkWindConstructor(ctxArray[i].ctx);
                                } else if (parksMap[park].parkType === 'SOLAR') {
                                    parkSolarConstructor(ctxArray[i].ctx);
                                } else {
                                    parkHydroConstructor(ctxArray[i].ctx);
                                }
                            }
                        }
                        //$('#' + id + 'ID #' + id + 'AP').html(ap);
                        //$('#' + id + 'ID #' + id + 'LF').html(lf);

                        if (map.getZoom() >= setParkCircleZoom) {
                            $('.park-label-' + id).remove();
                            createCircle(parksMap[park]);
                        }

                        for (var k = 0; k < popupsClick.length; k++) {
                            if (popupsClick[k].id === id) {
                                popupsClick[k].popup.setContent('<p><div id="' + parksMap[park].id + 'POP" style="box-shadow: 0 3px 14px rgba(0, 0, 0, 0.4); width: 143px; position: relative; margin-left: 36px; z-index: 400" href="callback:' + parksMap[park].id + '"><div style="color: white; background-color: black; width: 120px; margin-left: 40px; margin-top: -16px; padding-top: 1px;"><span style="padding-left: 14px">' + parksMap[park].name + '</span></div><div style="width: 124px; background-color: white; margin-left: 35px; font-size: 10.2px;"><span style="padding-left: 19px"> LF: <span id="' + parksMap[park].id + 'LF">' + Math.round(parksMap[park].lf) + '</span>% <span style="padding-left: 14px">AP: <span id="' + parksMap[park].id + 'AP">' + Math.round(parksMap[park].ap) + '</span>MW</span></div></div>')
                            }
                        }

                    } // PARKSMAP END
                }
            };

            function drawArc(ctx, start, size, color) {
                var radius = 23

                if (size <= 0) {
                    return;
                }

                var end = start + size;

                // adjust positions so the drawing starts at the top
                start = -0.5 + start;
                end = end > 2 ? 1.5 : end - 0.5;

                // add margin
                if ((color === '#D927BF' && start !== -0.5) ||
                    (end === 1.5 && start !== -0.5) ||
                    (end !== 1.5)) {
                    end -= 0.03;
                }

                start = start > end ? end : start;

                //ctx.translate(50 / 2, 50 / 2);
                ctx.beginPath();
                ctx.arc(50 / 2, 50 / 2, 21, 0, 2 * Math.PI);
                ctx.fillStyle = "white";
                ctx.fill();
                ctx.closePath();
                ctx.beginPath();

                ctx.arc(50 / 2, 50 / 2, radius, start * Math.PI, end * Math.PI);
                ctx.lineWidth = 4;
                ctx.strokeStyle = color;
                //ctx.fillStyle = "white";
                //ctx.fill();
                ctx.stroke();
                ctx.closePath();

                //ctx.translate(-50/2,-50/2);
            }

            function assetWindConstructor(ctx) {
                ctx.font = "25pt RMS";
                ctx.fillStyle = "black";
                ctx.textAlign = "middle";
                ctx.fillText("E", 9, 40);
            };

            function assetSolarConstructor(ctx) {
                ctx.font = "25pt RMS";
                ctx.fillStyle = "black";
                ctx.textAlign = "middle";
                ctx.fillText("F", 9, 40);
            };

            function assetHydroConstructor(ctx) {
                ctx.font = "25pt RMS";
                ctx.fillStyle = "black";
                ctx.textAlign = "middle";
                ctx.fillText("G", 9, 40);
            };

            function parkWindConstructor(ctx) {
                ctx.font = "25pt RMS";
                ctx.fillStyle = "black";
                ctx.textAlign = "middle";
                ctx.fillText("A", 9, 40);
            };

            function parkSolarConstructor(ctx) {
                ctx.font = "25pt RMS";
                ctx.fillStyle = "black";
                ctx.textAlign = "middle";
                ctx.fillText("B", 9, 40);
            };

            function parkHydroConstructor(ctx) {
                ctx.font = "25pt RMS";
                ctx.fillStyle = "black";
                ctx.textAlign = "middle";
                ctx.fillText("C", 9, 40);
            };

            function setUpParkForStatus(item, ctx, green, red) {
                var greenSize = green > 0 ? green : 0;
                var redSize = red > 0 ? red : 0;

                //if (firstDraw) {
                item.statusIndicator = {
                    green: greenSize,
                    red: redSize
                };
                ctx.clearRect(0, 0, 50, 50);

                ctx.beginPath();
                ctx.arc(50 / 2, 50 / 2, 24, 0, 2 * Math.PI);
                ctx.fillStyle = "white";
                ctx.fill();
                ctx.closePath();

                drawArc(ctx, 0, item.statusIndicator.green, '#39B54A');
                drawArc(ctx, item.statusIndicator.green, item.statusIndicator.red, '#C1272D');
                drawArc(ctx, item.statusIndicator.green + item.statusIndicator.red, 2 - (item.statusIndicator.green + item.statusIndicator.red), '#D927BF');
                //}
            };

            function getWeather(weather) {
                var weatherClass;
                switch (weather) {
                case 'Wind and Rain':
                    weatherClass = 'A';
                    break;
                case 'Wind and Clouds':
                    weatherClass = 'B';
                    break;
                case 'Snow Flakes':
                    weatherClass = 'C';
                    break;
                case 'Snow':
                    weatherClass = 'D';
                    break;
                case 'Clouds':
                    weatherClass = 'E';
                    break;

                case 'Partly Cloudy':
                    weatherClass = 'F';
                    break;

                case 'Mostly Cloudy':
                    weatherClass = 'G';
                    break;

                case 'Thunderstorm':
                    weatherClass = 'H';
                    break;

                case 'Drizzle':
                    weatherClass = 'I';
                    break;

                case 'Rain':
                    weatherClass = 'J';
                    break;

                case 'Sun':
                    weatherClass = 'K';
                    break;

                case 'Wind':
                    weatherClass = 'L';
                    break;

                case 'Fog':
                    weatherClass = 'M';
                    break;
                }
                return weatherClass;
            };

            var clicked = []

            function popupsManage(e) {
                if ($('#' + this.options.id + 'POP')[0]) {
                    if ($('#' + this.options.id + 'POP')[0].style.display !== 'none') {
                        $('#' + this.options.id + 'POP')[0].style.display = 'none';
                    } else {
                        $('#' + this.options.id + 'POP')[0].style.display = 'inline-block';
                    }

                }
            }

            // FOR MOBILE 
            function createOwner(id, name, lat, lon, radius, lf, ap) {
                owner = {
                    id: id,
                    lat: lat,
                    lon: lon,
                    radius: radius,
                    name: name,
                    ap: ap,
                    aP: {
                        tot: ap
                    },
                    lf: lf,
                    lF: {
                        tot: lf
                    },
                    type: 'owner'
                };
            };

			function createCountry(id, name, acronym, lat, lon, radius, lf, ap, online, offline, nocomm, weather, parentId) {
                countries['' + name + ''] = {
                    id: id,
                    lat: lat,
                    lon: lon,
					acc: acronym,
                    radius: radius,
                    color: '#FC7E3F',
					kpis: {circleStatus: { on: online, off: offline, noComm: nocomm}},
                    name: name,
                    ap: ap,
                    aP: {
                        tot: ap
                    },
                    lf: lf,
                    lF: {
                        tot: lf
                    },
                    weather: weather,
                    parentId: parentId,
                    type: 'country'
                }
            }
			
            function createRegion(id, name, acronym, lat, lon, radius, lf, ap, online, offline, nocomm, weather, parentId) {
                regions['' + name + ''] = {
                    id: id,
                    lat: lat,
					acc: acronym,
                    lon: lon,
                    radius: radius,
                    color: '#FC7E3F',
                    name: name,
					kpis: {circleStatus: { on: online, off: offline, noComm: nocomm}},
                    ap: ap,
                    aP: {tot: ap},
                    lf: lf,
                    lF: {tot: lf},
                    weather: weather,
                    parentId: parentId,
                    type: 'region'
                }
            };

            function createPark(id, name, energyType, lat, lon, radius, weather, online, offline, nocomm, lf, ap, parentId) {
                var assets = [];
                for (var asset in jAssets) {
                    if (jAssets[asset].parent === id) {
                        assets.push(jAssets[asset]);
                    }
                }

                parksMap['' + name + ''] = {
                    id: id,
                    name: name,
                    lat: lat,
                    lon: lon,
                    parkType: energyType,
                    ap: ap,
                    aP: {tot: ap},
                    lf: lf,
                    lF: {tot: lf},
                    radius: radius, //CircleRadiusCalculation(lat, lon, assets),
                    color: '#FC7E3F',
                    weather: weather,
                    parentId: parentId,
                    kpis: {
                        circleStatus: {
                            on: online,
                            off: offline,
                            noComm: nocomm
                        }
                    },
                    assets: assets,
                    type: 'park'
                }
                isReady = 1;
				
            };

            function createAsset(id, name, energyType, lat, lon, status, lf, ap, parentId) {
                jAssets['' + id + ''] = {
                    id: id,
                    name: id,
                    lat: lat,
                    lon: lon,
                    ap: ap,
                    aP: {tot: ap},
                    lf: lf,
                    lF: {tot: lf},
                    assetType: energyType,
                    parent: parentId,
                    status: status,
                }
            };
			
			var greenMarker;
			function createMarker(lat, lon) {
				 greenMarker = L.marker([lat, lon], {
    //icon: greenIcon,
    draggable: 'true'
}).bindPopup('<div style="background-color: white">' + lat + ', ' + lon + '</div>').addTo(map);

greenMarker.on('dragend', function (e) {
	greenMarker._popup.setContent('<div style="background-color: white; top: -15px; position:relative;">' + greenMarker.getLatLng().lat + ', ' + greenMarker.getLatLng().lng + '</div>');
})
			}

			function deleteMarker() {
				map.removeLayer(greenMarker);
			}
